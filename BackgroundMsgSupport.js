/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of Tincr.
*
* Tincr is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
var backgroundMsgSupport = {
	launchFolderSelect : function(index, url,  callback){
		chrome.extension.sendMessage({key: 'launchFolderSelect', index: index, url: url, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	launchFileSelect : function(url, op, callback){
		chrome.extension.sendMessage({key: 'launchFileSelect', url: url, op: op, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	getProjectTypes : function(callback){
		chrome.extension.sendMessage({key : 'ProjectTypes', tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	checkResources : function(resources, callback){
		chrome.extension.sendMessage({key : 'checkResources', resources: resources, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	checkResourceContent : function(url, content, callback){
		chrome.extension.sendMessage({key : 'checkResourceContent', url: url, content:content, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	updateResource : function(url, content, callback){
		chrome.extension.sendMessage({key : 'updateResource', url: url, content:content, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	pageChanged : function(callback){
		chrome.extension.sendMessage({key: 'pageChanged', tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	unwatchDirectory : function(callback){
		chrome.extension.sendMessage({key: 'unwatchDirectory', tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	loadProject : function(type, path, url, urlPathMap, callback){
		chrome.extension.sendMessage({key: 'loadProject', type: type, path: path, url: url, urlPathMap: urlPathMap, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	setResourceOptions : function(url, exactMatch, callback){
		chrome.extension.sendMessage({key: 'setResourceOptions', url: url, exactMatch: exactMatch, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	removeDependency : function(url, index, callback){
		chrome.extension.sendMessage({key: 'removeDependency', url: url, index: index, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	setCustomPathData : function(url, data, callback){
		chrome.extension.sendMessage({key: 'setCustomPathData', url:url, data: data, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	clearResource : function(url, callback){
		chrome.extension.sendMessage({key: 'clearResource', url:url, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	getPreference : function(prefKey, callback){
		chrome.extension.sendMessage({key:'getPreference', prefKey: prefKey, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	},
	setPreference : function(prefObj, callback){
		chrome.extension.sendMessage({key: 'setPreference', prefObj: prefObj, tabId : chrome.devtools.inspectedWindow.tabId}, callback);
	}
}
