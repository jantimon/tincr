/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of Tincr.
*
* Tincr is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
var isWin = navigator.platform.indexOf('Win') == 0;

var RubyOnRailsProject = function(){
	this.routes = [
					{from: new RegExp('/assets/(.+\\.js)'),
				     to: '/app/assets/javascripts/$1'},
					{from: new RegExp('/assets/(.+\\.css)'),
					 to: '/app/assets/stylesheets/$1'}
					]
	this.cachedUrlMap = {};
	this.matchedFileMap = {};
	if (isWin){
		this.replaceSlashes = function(str){
			return str.replace(/\//g, '\\');
		}
	}
	else{
		this.replaceSlashes = function(str){return str;};
	}
}

RubyOnRailsProject.prototype = {
	init : function(root, url, callback){
		var knownPaths = this.knownPaths = {};
		var directoriesToScan = ['/app/assets/javascripts', '/app/assets/stylesheets'];
		this.rootPath = root.fullPath;

		directoriesToScan.asyncEach(function(dirPath, done){
			ProjectUtils.scanDirectory(root, dirPath, function(file){
				knownPaths[file.fullPath] = true;
			}, done);
		}, callback);
	},
	_doRegexConversion : function(str, root, from, to){
		var match = str.match(from);
		if (match){
			var partialPath = match[0].replace(from, to);
			var path = root + (partialPath.charAt(0) == '/' ? '' : '/') + partialPath;
			return path;
		}
		return null;
	},
	filePathsForUrl : function(url){
		if (this.cachedUrlMap[url]){
			return this.cachedUrlMap[url];
		}
		if (url.indexOf('file://') == 0){
			return ProjectUtils.getFileUrlPaths(url, this.cachedUrlMap);
		}
		var routes = this.routes;
		for (var i = 0; i < routes.length; i++){
			var path = this._doRegexConversion(url, this.rootPath, routes[i].from, routes[i].to);
			if (path){
				var path = this.replaceSlashes(path);
				if (!this.knownPaths[path]){
					var exts = ['scss', 'coffee'];
					for (var j = 0; j < exts.length; j++){
						if (this.knownPaths[path + '.' + exts[j]]){
							var newPath = path + '.' + exts[j];
							var obj = {autoReload: [newPath]};
							this.cachedUrlMap[url] = obj;
							this.matchedFileMap[newPath] = url;
							return obj;
						}
					}
				}
				else{
					var obj = {autoSave: path};
					this.cachedUrlMap[url] = obj;
					this.matchedFileMap[path] = url;
					return obj;
				}
			}
		}
		return {};
	},
	urlsForFilePath : function(path){
		var single = this.matchedFileMap[path];
		if (single){
			return [single];
		}
		return [];
	},
	mapDataToUrl : function(url, obj){
		ProjectUtils.cacheRelationship(url, obj, false, this.cachedUrlMap, this.matchedFileMap);
	},
	clearUrlCache : function(url){
		ProjectUtils.clearUrlCache(url, false, this.cachedUrlMap, this.matchedFileMap);
	},
	resetUrls : function(){
		this.cachedUrlMap = {};
		this.matchedFileMap = {};
	}
}

ProjectTypes.push(
    {
		name: 'Ruby on Rails (3.1 or higher)',
		key: 'ror3.1',
		locationType : 'local',
		createProject : function(root, url, callback){
			
			var project = new RubyOnRailsProject();
			var trailer = /\s*;\s*$/;
			project.compare = function(remote,local){
				if (remote != local){
					// sprockets puts a semicolon and whitespace trailer on the end of js files. This tries to create 
					// a comparison that ignores the trailer.
					var idx = remote.indexOf(local);
					if (idx == 0){
						return remote.substring(local.length).search(trailer) == 0;
					}
				}
				else{
					return true;
				}
				return false;
			}
			
			project.init(root, url, function(){
				callback(project);
			});			
		} 
    }
);
